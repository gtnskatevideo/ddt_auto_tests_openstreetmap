from data import *
from conftest import *

url = 'https://nominatim.openstreetmap.org'

# def read_test_data_from_csv():
#     test_data = []
#     with open("test_data/data_street_coordinates.csv", encoding="utf8", newline="") as csvfile:
#         data = csv.reader(csvfile, delimiter=",")
#         next(data)
#         for row in data:
#             test_data.append(row)
#     return test_data

@allure.feature('Send address')
@allure.story('Отправка адреса для получения координат')
@pytest.mark.parametrize('q, lat, lon', testing_data_address)
def test_send_address(q, lat, lon):
    """
    Проверка получения координат после отправки адреса и сравнение координат с ожидаемым результатом 
    """

    response = ApiResponse(address=url).get_coordinate(search=q)
    
    with allure.step("Запрос отправлен, посмотрим код ответа"):
        assert response.status_code == 200, "Неверный код ответа, получен {response.status_code}"

    body = response.json()

    with allure.step("Сохраняем ответ в переменную и сравниваем координаты (предварительно проверяем количество ответов с адресами)"):
        body = response.json()
        if len(body) > 0:
            body = body[0]
            assert body['lat'] == lat
            assert body['lon'] == lon

        if len(body) == 0:
            print("Введены некорректные данные в запросе или адрес не найден")


@pytest.mark.parametrize('q, lat, lon', testing_data_address)
def test_send_coordinate(q, lat, lon):
    """
    Проверка получения адреса после отправки координат и сравнение адреса с ожидаемым результатом 
    """
    

    response = ApiResponse(address=url).get_coordinate(search=q)


    with allure.step("Запрос отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f'Ошибка на сервере или на клиенте. Статус код {response.status_code}'
    

    with allure.step("Запрос отправлен, проверим отображение адреса"):
        body = response.json()
        if len(body) > 0:
            body = body[0]
            print(body)
            assert q in body['display_name']
        if len(body) == 0: 
            assert pytest.fail, "Введены некорректные данные в запросе или адрес не найден"   

