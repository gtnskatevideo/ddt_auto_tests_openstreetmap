import requests
import pytest
import allure


class ApiResponse:

    def __init__(self, address):
        self.address = address

    def get_coordinate(self, path='/', search=None, format='json'):
        url = f'{self.address}{path}search?q={search}&format={format}'
        with allure.step(f'GET request to: {url}'):
            return requests.get(url)

    def get_address(self, path='/', lat=None, lon=None, format='json'):
        url = f'{self.address}{path}reverse?format={format}&lat={lat}&lon={lon}'
        with allure.step(f'GET request to: {url}'):
            return requests.get(url=url)
